package vemser.tests;

import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class ProdutoContratoTest {
    String urlLogin = "http://localhost/login";
    String bodyLoginADM = """
            {
              "email": "assucena@gmail.com",
              "password": "1233"
            }
            """;
    String tokenAdm;

    @BeforeEach
    public void setup() {
        baseURI = "http://localhost";
        port = 3000;

        tokenAdm =
                given()
                        .body(bodyLoginADM)
                        .contentType(ContentType.JSON)
                .when()
                        .post(urlLogin)
                .then()
                        .extract()
                        .path("authorization");


    }

    @Test
    public void testValidarContratoBuscarUsuarioPorIdValido() {
        String idProduto = "ZsDFZhFeQ02SnSpv";

        given()
                .pathParam("id", idProduto)
        .when()
                .get("/produtos/{id}")
        .then()
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath(
                        "schemas/buscar_produto_por_id.json"));
    }

    @Test
    public void testValidarContratoCadastrarProdutoComSucesso() {

        given()
                .contentType(ContentType.JSON)
                .header("Authorization", tokenAdm)
                .body(
                        """
                                {
                                  "nome": "teste 11233",
                                  "preco": 470,
                                  "descricao": "Mouse",
                                  "quantidade": 381
                                }
                                """)
        .when()
                .post("/produtos")
        .then()
                .body(JsonSchemaValidator.matchesJsonSchemaInClasspath(
                        "schemas/cadastrar_produto.json"));


    }

}
