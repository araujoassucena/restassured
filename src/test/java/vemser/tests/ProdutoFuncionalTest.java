package vemser.tests;

import client.CarrinhoClient;
import client.ProdutoClient;
import dataFactory.CarrinhoDataFactory;
import dataFactory.ProdutoDataFactory;
import model.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class ProdutoFuncionalTest {

    @Test
    public void testCadastrarProdutoComSucesso() {
        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then().log().all()

                        .statusCode(201).extract().as(ProdutoRes.class);

    assertEquals("Cadastro realizado com sucesso", cadastroProduto.getMessage());


        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then()
                        .statusCode(200);

    }


    @Test
    public void testTentarCadastrarProdutoComNomeJaUtilizado() {
        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoReq produtoNomeRepetido = ProdutoDataFactory.produto();
        produtoNomeRepetido.setNome(produto.getNome());

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes cadastroProdutoComNomeRepetido = ProdutoClient.cadastroProduto(produtoNomeRepetido)
                .then()
                        .statusCode(400).extract().as(ProdutoRes.class);

        assertEquals("Já existe produto com esse nome", cadastroProdutoComNomeRepetido.getMessage());


        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testTentarCadastrarProdutoComUsuarioNaoAdm() {
        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoRes cadastroProduto = ProdutoClient.cadastroProdutoNaoAdm(produto)
                .then()

                        .statusCode(403).extract().as(ProdutoRes.class);

        assertEquals("Rota exclusiva para administradores", cadastroProduto.getMessage());


    }

    @ParameterizedTest
    @MethodSource("dataProvider.ProdutoDataProvider#dataProviderCadastroInvalido")
    public void testTentarCadastrarProdutoInvalido(ProdutoReq produtoReq, String key, String value) {

        ProdutoClient.cadastroProduto(produtoReq)
                .then()

                .statusCode(400)
                .body(key, equalTo(value));

    }

    @Test
    public void testBuscarProdutoPorIdValido() {
        ProdutoReq produtoReq = ProdutoDataFactory.produto();

        ProdutoRes produtoRes = ProdutoClient.cadastroProduto(produtoReq)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes buscarProdutoPorIdRes = ProdutoClient.buscarProdutoPorId(produtoRes.get_id())

                .then()
                        .statusCode(200).extract().as(ProdutoRes.class);

        assertAll("buscarProdutoPorIdValido",
        () ->
        {
            assertEquals(produtoReq.getNome(), buscarProdutoPorIdRes.getNome());
            assertEquals(produtoReq.getDescricao(), buscarProdutoPorIdRes.getDescricao());
            assertEquals(produtoReq.getPreco(), buscarProdutoPorIdRes.getPreco());
            assertEquals(produtoReq.getQuantidade(), buscarProdutoPorIdRes.getQuantidade());

        });

        ProdutoClient.deletarProduto(produtoRes.get_id())
                .then().log().all()
                        .statusCode(200);
    }


    @ParameterizedTest
    @MethodSource("dataProvider.ProdutoDataProvider#dataProviderIdsInvalidos")
    public void testBuscarProdutoPorIdInvalido(String idInvalido, String resultadoEsperado) {
        ProdutoRes buscarProdutoPorIdInvalido = ProdutoClient.buscarProdutoPorId(idInvalido)
                .then()
                        .statusCode(400).extract().as(ProdutoRes.class);

        assertEquals(resultadoEsperado, buscarProdutoPorIdInvalido.getMessage());

    }

    @Test
    public void testEditarProdutoComSucesso() {
        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoReq produtoEditado = ProdutoDataFactory.produto();

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes produtoEditadoRes = ProdutoClient.editarProdutoPorId(produtoEditado, cadastroProduto.get_id())
                .then().log().all()
                        .statusCode(200).extract().as(ProdutoRes.class);

     assertEquals("Registro alterado com sucesso", produtoEditadoRes.getMessage());


        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                        .statusCode(200);

    }

    @Test
    public void testEditarProdutoComUmNomeJaUsado() {
        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoReq produtoParaEditar = ProdutoDataFactory.produto();
        ProdutoReq produtoNomeRepedito = ProdutoDataFactory.produto();
        produtoNomeRepedito.setNome(produto.getNome());

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes cadastroProdutoParaEditar = ProdutoClient.cadastroProduto(produtoParaEditar)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes produtoEditadoRes = ProdutoClient.editarProdutoPorId(produtoNomeRepedito, cadastroProdutoParaEditar.get_id())
                .then().log().all()
                        .statusCode(400).extract().as(ProdutoRes.class);

        assertEquals("Já existe produto com esse nome", produtoEditadoRes.getMessage());


        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                        .statusCode(200);

        ProdutoClient.deletarProduto(cadastroProdutoParaEditar.get_id())
                .then().log().all()
                        .statusCode(200);
    }

    @Test
    public void testTentarEditarProdutoNaoSendoUsuarioAdministrador() {

        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoReq produtoParaEditar = ProdutoDataFactory.produto();

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes prdutoParaEditarRes = ProdutoClient.editarProdutoPorIdNaoAdm(produtoParaEditar, cadastroProduto.get_id())
                .then()
                        .statusCode(403).extract().as(ProdutoRes.class);

 assertEquals("Rota exclusiva para administradores", prdutoParaEditarRes.getMessage());


        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                    .statusCode(200);
    }

    @Test
    public void testExcluirProdutoComSucesso() {
        ProdutoReq produto = ProdutoDataFactory.produto();

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                    .statusCode(201).extract().as(ProdutoRes.class);

        ProdutoRes produtoExcluirRes = ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                    .statusCode(200).extract().as(ProdutoRes.class);

assertEquals("Registro excluído com sucesso", produtoExcluirRes.getMessage());


    }

    @Test
    public void testTentarExcluirProdutoNaoSendoUsuarioAdministrador() {

        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoReq ecluirProdutoSemSerAdm = ProdutoDataFactory.produto();

        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then()
                    .statusCode(201).extract().as(ProdutoRes.class);


        ProdutoRes produtoExcluirRes = ProdutoClient.deletarProdutoNaoAdm(cadastroProduto.get_id())
                .then()
                    .statusCode(403).extract().as(ProdutoRes.class);

       assertEquals("Rota exclusiva para administradores", produtoExcluirRes.getMessage());

        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then().log().all()
                    .statusCode(200);

    }

    @Test
    public void testTentarExcluirProdutoAdicionadoAoCarrinho() {
        ProdutoReq produto = ProdutoDataFactory.produto();

        CarrinhoClient
                .excluirCarrinhoCancelandoCompra();

        ProdutoRes cadastroProduto = ProdutoClient
                .cadastroProduto(produto)
                .then()
                        .statusCode(201).extract().as(ProdutoRes.class);

        CarrinhoReq carrinhoReq = CarrinhoDataFactory
                .carrinhoReq(cadastroProduto.get_id(), produto.getQuantidade());

        CarrinhoClient.cadastroProdutoCarrinho(carrinhoReq)
                .then()
                        .statusCode(201);

        CarrinhoProdutoRes carrinhoProdutoRes =
        ProdutoClient.deletarProduto(cadastroProduto.get_id())
                .then()
                       .statusCode(400).extract().as(CarrinhoProdutoRes.class);

      assertEquals("Não é permitido excluir produto que faz parte de carrinho", carrinhoProdutoRes.getMessage());

        CarrinhoClient
                .excluirCarrinhoCancelandoCompra()
                .then()
                        .statusCode(200);

        ProdutoClient
                .deletarProduto(cadastroProduto.get_id())
                .then()
                        .statusCode(200);

    }


}

