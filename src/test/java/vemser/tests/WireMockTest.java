package vemser.tests;

import client.ProdutoClient;
import com.github.tomakehurst.wiremock.WireMockServer;
import dataFactory.ProdutoDataFactory;
import model.ProdutoReq;
import model.ProdutoRes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class WireMockTest {

    WireMockServer wireMockServer;

    @BeforeEach
    public void setup() {
        wireMockServer = new WireMockServer(3000);
        configureFor("localhost", 3000);
        wireMockServer.start();
        setupStub();

    }

    @AfterEach
    public void teardown() {
        wireMockServer.stop();
    }

    public void setupStub() {

        wireMockServer.stubFor(post(urlEqualTo("/produtos"))
                .withRequestBody(matchingJsonPath("nome", matching("^.+$")))
                .withRequestBody(matchingJsonPath("quantidade", matching("^[1-9]\\d*$")))
                .withRequestBody(matchingJsonPath("preco", matching("^[0-9]\\d*$")))
                .withRequestBody(matchingJsonPath("descricao", matching("^.+$")))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(201)
                        .withBodyFile("json/cadastrar_produto.json")));

        wireMockServer.stubFor(post(urlEqualTo("/login"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/login.json")));

        wireMockServer.stubFor(get(urlEqualTo("/produtos/jogfODIlXsqxNFS2"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(200)
                        .withBodyFile("json/buscar_produto.json")));

        wireMockServer.stubFor(get(urlEqualTo("/produtos/teste"))
                .willReturn(aResponse().withHeader("Content-Type", "application/json")
                        .withStatus(400)
                        .withBodyFile("json/buscar_produto_invalido.json")));
    }

    @Test
    public void testCadastrarProdutoComSucesso() {


        ProdutoReq produto = ProdutoDataFactory.produto();
        ProdutoRes cadastroProduto = ProdutoClient.cadastroProduto(produto)
                .then().log().all()

                .statusCode(201).extract().as(ProdutoRes.class);


        assertEquals("Cadastro realizado com sucesso", cadastroProduto.getMessage());


    }


    @ParameterizedTest
    @MethodSource("dataProvider.ProdutoDataProvider#dataProviderCadastroInvalido")
    public void testTentarCadastrarProdutoInvalido(ProdutoReq produtoReq, String key, String value) {

        String wireMockInvalido = "{" + "\"" + key + "\": \"" + value + "\"" + "}";

        wireMockServer.stubFor(post(urlEqualTo("/produtos"))
                .willReturn(
                        aResponse()
                                .withHeader("Content-Type", "application/json")
                                .withStatus(400)
                                .withBody(wireMockInvalido)));

        ProdutoClient.cadastroProduto(produtoReq)
                .then()
                .statusCode(400)
                .body(key, equalTo(value));

    }
  @Test
    public void testBuscarProdutoPorIdValido() {
        ProdutoReq produtoReq = ProdutoDataFactory.produto();

        ProdutoRes produtoRes = ProdutoClient.cadastroProduto(produtoReq)
                .then()
                .statusCode(201).extract().as(ProdutoRes.class);

  ProdutoClient.buscarProdutoPorId(produtoRes.get_id())

                .then()
                .statusCode(200);
    }

    @Test
    public void testBuscarProdutoPorIdInvalido() {


        ProdutoClient.buscarProdutoPorId("teste")

                .then()
                .statusCode(400);
    }

}
