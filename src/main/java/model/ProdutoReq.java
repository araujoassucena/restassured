package model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ProdutoReq {
    private String nome;
    private String preco;
    private String descricao;
    private String quantidade;



    @Override
    public String toString() {
        return "{"
                + "\"nome\":\"" + nome + "\","
                + "\"preco\":\"" + preco + "\","
                + "\"descricao\":\"" + descricao + "\","
                + "\"quantidade\":\"" + quantidade + "\""
                + "}";
    }

}
