package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarrinhoProdutoReq {

    private String idProduto;
    private Integer quantidade;

    @Override
    public String toString() {
        return "{" +
                "idProduto='" + idProduto + '\'' +
                ", quantidade=" + quantidade +
                '}';
    }

}
