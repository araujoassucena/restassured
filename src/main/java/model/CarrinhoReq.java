package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
public class CarrinhoReq {
    private ArrayList<CarrinhoProdutoReq> produtos;

    public CarrinhoReq (){
        produtos = new ArrayList<>();
    }

}
