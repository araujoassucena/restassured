package specs;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*;
import static io.restassured.config.LogConfig.logConfig;

public class  InicialSpecs {

    private InicialSpecs() {}

    public static RequestSpecification setup() {
        return new RequestSpecBuilder()
                .setBaseUri("https://serverest.dev/#/")
                .setPort(3000)
                .setConfig(RestAssured.config().logConfig(logConfig().enableLoggingOfRequestAndResponseIfValidationFails()))
                .build();
    }
}
