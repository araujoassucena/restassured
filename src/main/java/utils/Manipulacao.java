package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Manipulacao {

    private Manipulacao (){}

    public static Properties getProp() {
        Properties props = new Properties();
        try {
            FileInputStream file = new FileInputStream("src/properties/dados.properties");
            props.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }
}
