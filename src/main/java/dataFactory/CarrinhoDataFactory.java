package dataFactory;

import model.CarrinhoProdutoReq;
import model.CarrinhoReq;
import net.datafaker.Faker;

import java.util.Locale;

public class CarrinhoDataFactory {
    static Faker faker = new Faker(new Locale("PT-BR"));
    public static CarrinhoReq carrinhoReq (String idDoProduto, String quantidade){
        CarrinhoProdutoReq carrinhoProdutoReq = new CarrinhoProdutoReq();
        carrinhoProdutoReq.setQuantidade(Integer.valueOf(quantidade));
        carrinhoProdutoReq.setIdProduto(idDoProduto);
        CarrinhoReq carrinhoReq = new CarrinhoReq();
        carrinhoReq.getProdutos().add(carrinhoProdutoReq);

        return carrinhoReq;

    }



}
