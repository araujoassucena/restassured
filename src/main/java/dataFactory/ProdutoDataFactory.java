package dataFactory;

import model.ProdutoReq;
import net.datafaker.Faker;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class ProdutoDataFactory {


    public static Object produtoComLetraNaDescricao;
    static Faker faker = new Faker(new Locale("PT-BR"));
    public static ProdutoReq produto() {
        ProdutoReq produto = new ProdutoReq();
        produto.setNome(faker.commerce().productName());
        produto.setPreco(String.valueOf(faker.number().positive()));
        produto.setDescricao(faker.lorem().sentence());
        produto.setQuantidade(String.valueOf(faker.number().positive()));
        return produto;
    }

    public static ProdutoReq produtoNomeRepetido() {
        ProdutoReq produto = new ProdutoReq();
        produto.setNome(faker.commerce().productName());
        produto.setPreco(String.valueOf(faker.number().positive()));
        produto.setDescricao(faker.lorem().sentence());
        produto.setQuantidade(String.valueOf(faker.number().positive()));

        return produto;
    }


    public static ProdutoReq produtoComNomeVazio() {
        ProdutoReq produtoReq = produto();
        produtoReq.setNome(StringUtils.EMPTY);
        return produtoReq;

    }

    public static ProdutoReq produtoComPrecoMenorQueZero(){
        ProdutoReq produtoReq = produto();
        produtoReq.setPreco("-10");
        return produtoReq;

    }

    public static ProdutoReq produtoComLetraNoPreco (){
        ProdutoReq produtoReq = produto();
        produtoReq.setPreco("teste");
        return produtoReq;

    }

    public static ProdutoReq produtoComDescricaoVazio(){
        ProdutoReq produtoReq = produto();
        produtoReq.setDescricao(StringUtils.EMPTY);
        return produtoReq;

    }

    public static ProdutoReq produtoComQuantidadeMenorQueZero(){
        ProdutoReq produtoReq = produto();
        produtoReq.setQuantidade("-10");
        return produtoReq;
    }
    public static ProdutoReq produtoComLetraNaQuantidade(){
        ProdutoReq produtoReq = produto();
        produtoReq.setQuantidade("teste");
        return produtoReq;

    }

    public static String produtoIdInvalidoComNumeros() {

        return "12345678";

    }

    public static String produtoIdInvalidoComLetras() {
        return "idInvalido";

    }

    public static String produtoIdInvalidoComEspacos() {
        return "        ";
    }
}
